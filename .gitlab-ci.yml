image: docker:19.03.12
services:
    - docker:19.03.12-dind

stages:
    - build
    - static-analysis
    - test
    - release

variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    CONTAINER_COMMIT_API_IMAGE: $CI_REGISTRY_IMAGE:api-$CI_COMMIT_SHORT_SHA
    CONTAINER_COMMIT_WEB_IMAGE: $CI_REGISTRY_IMAGE:web-$CI_COMMIT_SHORT_SHA
    CONTAINER_LATEST_API_IMAGE: $CI_REGISTRY_IMAGE:api-latest
    CONTAINER_LATEST_WEB_IMAGE: $CI_REGISTRY_IMAGE:web-latest
    CONTAINER_RELEASE_API_IMAGE: $CI_REGISTRY_IMAGE:api-$CI_COMMIT_TAG
    CONTAINER_RELEASE_WEB_IMAGE: $CI_REGISTRY_IMAGE:web-$CI_COMMIT_TAG


before_script:
    - docker -v
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - echo Test images are $CONTAINER_COMMIT_API_IMAGE and $CONTAINER_COMMIT_WEB_IMAGE

build:
    stage: build
    script:
        - |
            set +e
            echo Checking registry for image $CONTAINER_COMMIT_API_IMAGE
            docker pull $CONTAINER_COMMIT_API_IMAGE
            if [[ $? -eq 0 ]]
            then 
                echo Image $CONTAINER_COMMIT_API_IMAGE exists, skip building.
            else
                echo Image $CONTAINER_COMMIT_API_IMAGE does not exist, building...
                docker build --pull -t $CONTAINER_COMMIT_API_IMAGE .
                export BUILD_RESULT=$?
                if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
                docker push $CONTAINER_COMMIT_API_IMAGE
                export BUILD_RESULT=$?
                if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
            fi
            echo Checkig registry for image $CONTAINER_COMMIT_WEB_IMAGE
            docker pull $CONTAINER_COMMIT_WEB_IMAGE
            if [[ $? -eq 0 ]]
            then 
                echo Image $CONTAINER_COMMIT_WEB_IMAGE exists, skip building.
            else
                echo Image $CONTAINER_COMMIT_WEB_IMAGE does not exist, building...
                docker build --pull -t $CONTAINER_COMMIT_WEB_IMAGE ./web
                export BUILD_RESULT=$?
                if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
                docker push $CONTAINER_COMMIT_WEB_IMAGE
                export BUILD_RESULT=$?
                if [[ $BUILD_RESULT -ne 0 ]]; then exit $BUILD_RESULT; fi
            fi
        - exit $BUILD_RESULT
    except:
        - tags


test-lint:
    stage: static-analysis
    script:
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker run --name lint-tests $CONTAINER_COMMIT_API_IMAGE pylint app/ | tee pylint_result.txt
    artifacts:
        paths:
            - pylint_result.txt


include:
    - template: Dependency-Scanning.gitlab-ci.yml

dependency_scanning:
    stage: static-analysis
    variables:
        PIP_REQUIREMENTS_FILE: "requires"
    before_script:
        - echo "Starting dependency scanning"
        
test-unit:
    stage: test
    script:
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker run --name unit-tests $CONTAINER_COMMIT_API_IMAGE pytest --cov --cov-report=term --cov-report=xml:results/coverage.xml --junit-xml=results/unit_result.xml -m unit
        - docker cp unit-tests:/opt/calc/results ./
    coverage: '/^TOTAL.+?(\d+\%)$/'
    artifacts:
        reports:
            junit: results/unit_result.xml
            cobertura: results/coverage.xml


test-api:
    stage: test
    script:
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker network create calc-test-api
        - docker run --network calc-test-api --name apiserver -d -p 5000:5000 $CONTAINER_COMMIT_API_IMAGE flask run
        - docker run --network calc-test-api --name api-tests --env BASE_URL=http://apiserver:5000/ $CONTAINER_COMMIT_API_IMAGE pytest --junit-xml=results/api_result.xml -m api
        - docker cp api-tests:/opt/calc/results ./
        - docker rm --force apiserver || true
        - docker rm --force api-tests || true
        - docker network rm calc-test-api || true
    artifacts:
        reports:
            junit: results/api_result.xml

test-e2e:
    stage: test
    script:
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker pull $CONTAINER_COMMIT_WEB_IMAGE
        - docker network create calc-test-e2e
        - docker run --network calc-test-e2e --name apiserver -p 5000:5000 -d $CONTAINER_COMMIT_API_IMAGE flask run
        - docker run --network calc-test-e2e --name calc-web -p 80:80 -d $CONTAINER_COMMIT_WEB_IMAGE
        - docker create --network calc-test-e2e --name e2e-tests cypress/included:4.9.0 --browser chrome
        - docker cp ./test/e2e/cypress.json e2e-tests:/cypress.json
        - docker cp ./test/e2e/cypress e2e-tests:/cypress
        - docker start -a e2e-tests || TEST_RESULT=$?
        - echo E2E exit code was $TEST_RESULT 
        - docker cp e2e-tests:/results ./
        - docker cp e2e-tests:/cypress/screenshots ./
        - docker cp e2e-tests:/cypress/videos ./
        - docker logs apiserver > apiserver.txt 2>&1
        - docker logs calc-web > calc-web.txt 2>&1
        - docker rm --force apiserver  || true
        - docker rm --force calc-web || true
        - docker rm --force e2e-tests || true
        - docker network rm calc-test-e2e || true
        - exit $TEST_RESULT
    artifacts:
        when: always
        paths:
            - screenshots
            - videos
            - apiserver.txt
            - calc-web.txt
        reports:
            junit: results/cypress_result.xml

      
release-latest:
    stage: release
    script:
        - echo Test images are $CONTAINER_COMMIT_API_IMAGE and $CONTAINER_COMMIT_WEB_IMAGE
        - echo Latest images are $CONTAINER_LATEST_API_IMAGE and $CONTAINER_LATEST_WEB_IMAGE
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker pull $CONTAINER_COMMIT_WEB_IMAGE
        - docker tag $CONTAINER_COMMIT_API_IMAGE $CONTAINER_LATEST_API_IMAGE
        - docker tag $CONTAINER_COMMIT_WEB_IMAGE $CONTAINER_LATEST_WEB_IMAGE
        - docker push $CONTAINER_LATEST_API_IMAGE
        - docker push $CONTAINER_LATEST_WEB_IMAGE
    only:
        - master

release-tag:
    stage: release
    script:
        - echo Test images are $CONTAINER_COMMIT_API_IMAGE and $CONTAINER_COMMIT_WEB_IMAGE
        - echo Release images are $CONTAINER_RELEASE_API_IMAGE and $CONTAINER_RELEASE_WEB_IMAGE
        - docker pull $CONTAINER_COMMIT_API_IMAGE
        - docker pull $CONTAINER_COMMIT_WEB_IMAGE
        - docker tag $CONTAINER_COMMIT_API_IMAGE $CONTAINER_RELEASE_API_IMAGE
        - docker tag $CONTAINER_COMMIT_WEB_IMAGE $CONTAINER_RELEASE_WEB_IMAGE
        - docker push $CONTAINER_RELEASE_API_IMAGE
        - docker push $CONTAINER_RELEASE_WEB_IMAGE
    only:
        - tags
